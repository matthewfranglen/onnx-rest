Onnx Runner
===========

This is a simple Java 19 application that can load an ONNX model and perform inference.
To run this you need an ONNX model that accepts the arguments `input_ids` and `attention_mask`, and returns the argument `logits`.

Building and Running
--------------------

You can use `mvnw` to build the jar:

```bash
./mvnw clean package -Dmaven.test.skip
```

If you want to run the tests you need to have a valid model file as `model.onnx` in the root of the project.

And you can then run it.
The `--model.path` argument is how you specify the exported onnx file:

```bash
java -jar target/onnx-rest-0.0.1-SNAPSHOT.jar --model.path=file:./model.onnx
```

Inference
---------

The app provides a REST interface for the model.
It starts on port 8080 and you can invoke it as follows:

```bash
➜ curl -X POST -d '{"inputIds": [[0]], "attentionMask": [[1]]}' -H 'Content-type:application/json' http://localhost:8080/inference
{"logits":[[-0.22741635,0.46808624,-0.7372831]]}
```
