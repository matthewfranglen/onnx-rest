package com.matthew.onnxrest.controller;

import ai.onnxruntime.OrtException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@SpringBootTest
public class InferenceControllerTests {

    @Autowired
    private InferenceController controller;

    @Test
    public void testInference() throws OrtException {
        var request = new InputIdsTokenTypesAttentionMaskRequest(new long[][]{{0L}}, new long[][]{{0L}}, new long[][]{{1L}});
        var response = controller.tokenInference(request);
        assertThat(response, is(notNullValue()));
    }

}
