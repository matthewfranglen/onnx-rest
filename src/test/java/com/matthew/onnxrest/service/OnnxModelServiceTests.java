package com.matthew.onnxrest.service;

import ai.onnxruntime.OrtException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@SpringBootTest
public class OnnxModelServiceTests {

    @Autowired
    private OnnxModelService service;

    @Test
    public void testInference() throws OrtException {
        var response = service.tokenInference(
                new long[][]{{0L}},
                new long[][]{{0L}},
                new long[][]{{1L}}
        );
        assertThat(response, is(notNullValue()));
    }

}
