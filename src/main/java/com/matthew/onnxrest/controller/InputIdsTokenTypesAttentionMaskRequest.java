package com.matthew.onnxrest.controller;

public record InputIdsTokenTypesAttentionMaskRequest(long[][] inputIds, long[][] tokenTypeIds, long[][] attentionMask) {
}
