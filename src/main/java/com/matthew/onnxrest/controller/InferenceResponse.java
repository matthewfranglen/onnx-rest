package com.matthew.onnxrest.controller;

public record InferenceResponse(float[][] logits) {
}
