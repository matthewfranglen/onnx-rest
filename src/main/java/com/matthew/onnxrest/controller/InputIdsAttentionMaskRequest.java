package com.matthew.onnxrest.controller;

public record InputIdsAttentionMaskRequest(long[][] inputIds, long[][] tokenTypeIds, long[][] attentionMask) {
}
