package com.matthew.onnxrest.controller;

import ai.onnxruntime.OrtException;
import com.matthew.onnxrest.service.OnnxModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class InferenceController {

    private static final Logger logger = LoggerFactory.getLogger(InferenceController.class);
    private final OnnxModelService modelService;

    InferenceController(OnnxModelService modelService) {
        this.modelService = modelService;
    }

    @PostMapping("/inference")
    public InferenceResponse tokenInference(@RequestBody InputIdsTokenTypesAttentionMaskRequest request) throws OrtException {
        logger.info("Performing inference over {} sequences", request.inputIds().length);
        float[][] logits = modelService.tokenInference(
                request.inputIds(),
                request.tokenTypeIds(),
                request.attentionMask()
        );
        return new InferenceResponse(logits);
    }

    @PostMapping("/inference/3arg")
    public InferenceResponse threeArgumentInference(@RequestBody InputIdsTokenTypesAttentionMaskRequest request) throws OrtException {
        logger.info("Performing inference over {} sequences", request.inputIds().length);
        float[][] logits = modelService.tokenInference(
                request.inputIds(),
                request.tokenTypeIds(),
                request.attentionMask()
        );
        return new InferenceResponse(logits);
    }

    @PostMapping("/inference/2arg")
    public InferenceResponse twoArgumentInference(@RequestBody InputIdsAttentionMaskRequest request) throws OrtException {
        logger.info("Performing inference over {} sequences", request.inputIds().length);
        float[][] logits = modelService.tokenInference(
                request.inputIds(),
                request.attentionMask()
        );
        return new InferenceResponse(logits);
    }

}
