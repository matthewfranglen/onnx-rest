package com.matthew.onnxrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnnxRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnnxRestApplication.class, args);
    }

}
