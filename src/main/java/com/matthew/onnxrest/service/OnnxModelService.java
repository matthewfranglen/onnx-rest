package com.matthew.onnxrest.service;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtEnvironment;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class OnnxModelService {

    private static final Logger logger = LoggerFactory.getLogger(OnnxModelService.class);
    private final OrtEnvironment environment;
    private final OrtSession session;

    public OnnxModelService(@Value("${model.file}") final Resource modelFile) throws OrtException, IOException {
        logger.info("Loading model from {}", modelFile);
        environment = OrtEnvironment.getEnvironment();
        session = environment.createSession(modelFile.getFile().getAbsolutePath(), new OrtSession.SessionOptions());
    }

    public float[][] tokenInference(long[][] inputIds, long[][] tokenTypeIds, long[][] attentionMask) throws OrtException {
        OnnxTensor inputIdsTensor = OnnxTensor.createTensor(environment, inputIds);
        OnnxTensor tokenTypeIdsTensor = OnnxTensor.createTensor(environment, tokenTypeIds);
        OnnxTensor attentionMaskTensor = OnnxTensor.createTensor(environment, attentionMask);
        Map<String, OnnxTensor> inputs = new HashMap<>();
        inputs.put("input_ids", inputIdsTensor);
        inputs.put("token_type_ids", tokenTypeIdsTensor);
        inputs.put("attention_mask", attentionMaskTensor);
        OrtSession.Result response = session.run(inputs);
        return (float[][]) response.get(0).getValue();
    }

    public float[][] tokenInference(long[][] inputIds, long[][] attentionMask) throws OrtException {
        OnnxTensor inputIdsTensor = OnnxTensor.createTensor(environment, inputIds);
        OnnxTensor attentionMaskTensor = OnnxTensor.createTensor(environment, attentionMask);
        Map<String, OnnxTensor> inputs = new HashMap<>();
        inputs.put("input_ids", inputIdsTensor);
        inputs.put("attention_mask", attentionMaskTensor);
        OrtSession.Result response = session.run(inputs);
        return (float[][]) response.get(0).getValue();
    }

}
